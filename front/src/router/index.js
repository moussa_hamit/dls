import Vue from 'vue'
import VueRouter from 'vue-router'
import UserList from '../components/UserList.vue'
import AddUser from "../components/AddUser.vue";
import UpdateUser from "../components/UpdateUser.vue";
import VoitureList from "../components/VoitureList.vue";
import AddVoiture from "../components/AddVoiture.vue";
import UpdateVoiture from "../components/UpdateVoiture.vue";
Vue.use(VueRouter)

const routes = [
  {
    path: '/users',
    name: 'user.index',
    component: UserList
  },
  {
    path: '/users/new',
    name: 'user.add',
    component: AddUser
  },
  {
    path: '/users/update/:id',
    name: 'user.update',
    component: UpdateUser,
    props: true
  },
  {
    path: '/users/voitures/:userId',
    name: 'user.voitures',
    component: VoitureList,
    props: true
  },
  {
    path: '/voitures/:userId',
    name: 'user.voitures.add',
    component: AddVoiture,
    props: true
  },
  {
    path: '/voitures/update/:userId/:voitureId',
    name: 'user.voitures.update',
    component: UpdateVoiture,
    props: true
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
