import Vue from 'vue'
import router from './router'
import App from './App.vue'
Vue.config.productionTip = false
const app = new Vue({
  render: h => h(App),
  router:router,
  components:{App}
}).$mount('#app')

export {app}