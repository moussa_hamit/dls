# Création de la base de données
Executer le code sql se trouvant dans le fichier [db.sql](/apidlsante/db.sql) dans un terminal mysql ou l'importer avec l'outil de votre choix.
# Modifier les informations du serveur de  base de données
Mettre à jour le mot de passe, le nom d'utilisateur de la BD ainsi que le port utilisé dans le fichier [app.module.ts](/apidlsante/src/app.module.ts)

# Installer les packages
Dans chacun des deux projets lancer la commande
```
npm install
```
# Lancer le server nestjs 
```
npm run start:dev
ou
npm run start
```
# Lancer l'application front vuejs
```
npm run serve
```
# et profiter