import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { UsersEntity } from './users/users.entity';
import { VoituresModule } from './voitures/voitures.module';
import { VoituresEntity } from './voitures/voitures.entity';


@Module({
  imports: [TypeOrmModule.forRoot(
    {
      "type": "mysql",
      "host": "localhost",
      "port": 3306,
      "username": "root",
      "password": "",
      "database": "db",
      "synchronize": false,
      "logging": true,
      "entities": [UsersEntity,VoituresEntity]
      
    }
  ),UsersModule, VoituresModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  
}
