import { Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import {UsersEntity} from '../users/users.entity';

@Entity('voitures')
export class VoituresEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  marque: string;

  @Column()
  modele: string;

  @ManyToOne(() => UsersEntity, user => user.voitures)
  user: UsersEntity;

}