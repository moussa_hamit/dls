import { Module } from '@nestjs/common';
import { VoituresEntity } from './voitures.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoituresService } from './voitures.service';
import { VoituresController } from './voitures.controller';

@Module({
    imports: [TypeOrmModule.forFeature([VoituresEntity])],
    providers: [VoituresService],
    controllers: [VoituresController]
})
export class VoituresModule {}
