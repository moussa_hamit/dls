import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { createQueryBuilder, Repository } from 'typeorm';

import { VoituresEntity } from './voitures.entity';
import { VoituresDTO } from './voitures.dto';

@Injectable()
export class VoituresService {
  constructor(
    @InjectRepository(VoituresEntity)
    private voituresRepository: Repository<VoituresEntity>,
  ) {}

  async showAll() {
    return await this.voituresRepository.find();
  }

  async showPerUser(userId: Number) {
    return await this.voituresRepository.createQueryBuilder().where('userId= :userId', {userId: userId}).getMany()
  }
  async create(data: VoituresDTO) {
    const voiture = this.voituresRepository.create(data);
    await this.voituresRepository.save(data);
    return voiture;
  }


  async read(id: number) {
    return await this.voituresRepository.findOne({ where: { id: id } });
  }

  async update(id: number, data: Partial<VoituresDTO>) {
    await this.voituresRepository.update({ id }, data);
    return await this.voituresRepository.findOne({ id });
  }

  async destroy(id: number) {
    await this.voituresRepository.delete({ id });
    return { deleted: true };
  }
}
